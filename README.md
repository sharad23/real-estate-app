# Toptal project

## About the project

The project conatins two modules one is the Auth for authentication and Properties for  the apartments. There are two level of user realtor and user. The realtor can create, update and delete properties. The User can search them according to their search citera.
Currently the user can search by geo_location and price_per_month. Details are explained in this task description [link](https://www.toptal.com/screening_wizard/project_preview?utm_medium=email)

## Video Recording
Check my video recoding explaining about the project here
[Video Recoding](https://drive.google.com/file/d/1Ue3e-t9CdRR9zY-pyDiH_gONmJ4gKavX/view?usp=sharing)


### Architecture

The express api are publicly exposed. All the data are stored on Mysql DB. All the search regarding the apartments are conducted through elasticsearch. Syncing between two Databases is done asynchronously via rabbitMq. 
Whenever there is a write on the apartments(properties) table, a message is also published to rabbitMQ for the change. There is also a worker which is subscribing the Queue. As soon as the message is published to the queue, the worker consumes that message sync the elasticsearch. 
For some reason if the elasticsearch fails or anything, it will fall back to mysql for read. As a easlticsearch is designed for search, it gives us more flexibilty.On the long run, it much more easier to scale on elasticsearch.

Here, is the [link](https://drive.google.com/file/d/1h_kv9EBAHjQ8Ys6OstSsZOizGC5xjHwx/view?usp=sharing) to architecture diagram to make it more clearer. 


## Installation
Al the components express api, mysql, elasticsearch, rabitmq and worker are installed via docker-compose file.
Note: For demo purposes the docker conatiner are not mounted to persistant volume. So, you will loose data once the conatiners are stopped.
To install and run this project, follow these steps:
You just need to have a docker runtime installed

1. Clone this repository:

```bash
git clone <repository>
```

2. Build the containers
```bash
  docker-compose build
```

3. Run the containers
```bash
  docker-compose up
```

After the conatiners are booted. You can access the api at [http://localhost:3000](http://localhost:3000). 

The postman documentation for the api is at this [link](https://documenter.getpostman.com/view/19846536/2sA2rFRKpC
