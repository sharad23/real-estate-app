const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const routes = require('./src/routes');
const cors = require('cors');
const config = require('./src/config');
const path = require('path');
const logger = require('./src/lib/logger');
const sequelize = require('./src/db');

const app = express();

app.use(cors());
app.use(cookieParser());
app.use(bodyParser.json());

// serving uploaded files
app.use('/uploads', express.static(path.join(__dirname, config.uploadDirectory)));

// routes
app.use('/', routes);

app.use((err, req, res, next) => {
  logger.error(`Error: ${err.message}`)
  res.status(err.status || 500);
  // Send back an error response with the error message
  res.json({
      error: {
          message: err.message
      }
  });
});

// Start the server
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  logger.info(`Server is running on port ${PORT}`);
});

// Graceful shutdown
process.on('SIGINT', async () => {
  logger.info('Received SIGINT. Shutting down gracefully...');
  await sequelize.close();
  process.exit(0)
});
