const { Sequelize } = require('sequelize');
const config = require('./config');

const sequelize = new Sequelize('real_state_db', config.db.username, config.db.password, {
  host: config.db.host,
  port: config.db.port,
  dialect: 'mysql',
  pool: {
    max: 10, 
    min: 0, 
    acquire: 30000, 
    idle: 10000
  }
});

module.exports = sequelize