const Joi = require('joi');
const config = require('../config');
const _ = require('lodash');

const authRegisterPostSchema = Joi.object({
  name: Joi.string().trim().min(3).max(20).regex(/^[a-zA-Z]+$/).required(),
  email: Joi.string().trim().email().required(),
  password: Joi.string().min(8).trim().required(),
  confirm_password: Joi.string().valid(Joi.ref('password')).required().messages({
    'any.only': 'Passwords do not match'
  }),
  role: Joi.string().trim().valid(..._.values(config.roles)).required()
});

const authLoginPostSchema = Joi.object({
  email: Joi.string().email().trim().required(),
  password: Joi.string().min(8).trim().required(),
});

const propertiesPostSchema = Joi.object({
  name: Joi.string().trim().min(1).max(50).regex(/^[a-zA-Z\s]+$/).required(),
  number_of_rooms: Joi.number().integer().positive().optional(),
  area: Joi.number().positive().required(),
  longitude: Joi.number().min(-180).max(180).required(),
  latitude: Joi.number().min(-90).max(90).required(),
  description: Joi.string().min(1).max(1000).trim().optional(),
  tags: Joi.string().trim().optional(),
  price_per_month: Joi.number().positive().optional()    
})

const propertiesPutSchema = Joi.object({
  name: Joi.string().trim().min(1).max(50).regex(/^[a-zA-Z\s]+$/).optional(),
  number_of_rooms: Joi.number().integer().positive().optional(),
  area: Joi.number().positive().optional(),
  longitude: Joi.number().min(-180).max(180).optional(),
  latitude: Joi.number().min(-90).max(90).optional(),
  description: Joi.string().min(1).max(1000).trim().optional(),
  tags: Joi.string().trim().optional(),
  price_per_month: Joi.number().positive().optional()    
})

const searchSchema = Joi.object({
  geo: Joi.object({
    longitude: Joi.number().min(-180).max(180).required(),
    latitude: Joi.number().min(-90).max(90).required(),
    distance: Joi.number().integer().min(0).required(),
    unit: Joi.string().valid('km', 'mi').optional()
  }),
  price_per_month: Joi.object({
    minimum: Joi.number().integer().min(0),
    maximum: Joi.number().integer().min(0)
  })
})

module.exports =  {
  authRegisterPostSchema,
  authLoginPostSchema,
  propertiesPostSchema,
  propertiesPutSchema,
  searchSchema
}