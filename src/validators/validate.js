const createError = require('http-errors');
const { BAD_REQUEST } = require('../helpers/statusCodes')

const validate = (validationSchema, data) => { 
  const { error, value } = validationSchema.validate(data);
  if (error) {
    throw createError(BAD_REQUEST, error.details[0].message)
  }
  return value
}

module.exports = validate