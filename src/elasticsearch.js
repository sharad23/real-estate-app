const { Client } = require('@elastic/elasticsearch');
const logger = require('./lib/logger');
const config = require('./config');

const client = new Client({ node: `http://${config.elasticSearch.host}:${config.elasticSearch.port}` });
const indexName = 'apartments';
const mappings = {
  properties: {
    coordinate: {
      type: 'geo_point',
    },
    price_per_month: {
      type: 'float',
    },
    number_of_rooms: {
      type: 'integer',
    },
    name: {
      type: 'text',
      index: false,
    },
    area: {
      type: 'float',
      index: false,
    },
    images : {
      type: 'keyword',
      index: false
    },
    tags: {
      type: 'keyword',
    },
  },
};

async function checkElasticsearchHealth() {
  try {
    await client.ping();
    logger.info('Elasticsearch is reachable');
    return true
  } catch (error) {
    logger.error(`Elasticsearch is unreachable`)
    return false; // Return 'red' status if there's an error
  }
}

async function createIndex() {
  try {
    await client.indices.create({
      index: indexName,
      body: {
        mappings: {
          properties: mappings.properties,
        },
      },
    });
    logger.info('Index created successfully');
  } catch (error) {
    logger.error(`Index creation error ${error.message}`)
  }
}

module.exports =  {
  checkElasticsearchHealth,
  createIndex,
  client,
  indexName
}