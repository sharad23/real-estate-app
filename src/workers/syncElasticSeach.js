const amqp = require('amqplib');
const elasticsearch = require('../lib/elasticsearch');
const logger = require('../lib/logger');
const config = require('../config');
const { sleep } = require('../helpers/utils');

async function receive() {
  try {
    // Connect to RabbitMQ server
    const connection = await amqp.connect(`amqp://${config.rabbitMq.host}`);
    // Create a channel
    const channel = await connection.createChannel();

    // Declare a queue
    const queueName = config.rabbitMq.queueName;
    await channel.assertQueue(queueName, { durable: false });

    // Consume messages from the queue
    logger.info('Waiting for messages.....');
    
    channel.consume(queueName, async (message) => {
      logger.info(`[x] Received '${message.content.toString()}'`);
      const messageJson = JSON.parse(message.content.toString())
      
      if (messageJson.method == 'create') {
        await elasticsearch.createRecord(messageJson.uid, messageJson.payload)
      }

      if (messageJson.method == 'delete') {
        await elasticsearch.deleteRecord(messageJson.uid)
      }

      if (messageJson.method == 'update') {
        await elasticsearch.updateRecord(messageJson.uid, messageJson.payload)
      }
    }, { noAck: true }); // Set noAck to true for automatic acknowledgment

  } catch (error) {
    logger.error('Error receiving message:', error);
  }
}

(async() => {
    await sleep(30000)
    logger.info('Worker starting')
    await receive()
})()