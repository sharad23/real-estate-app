const jwt = require('jsonwebtoken');
const config = require('../config');
const createError = require('http-errors');
const httpStatus = require('../helpers/statusCodes');
const errorMessages = require('../helpers/errorMessages');


const protected = (req, res, next) => {
  if (req.cookies.token) {
    jwt.verify(req.cookies.token, config.salt, (err, decoded) => {
      if (err) {
        throw createError(httpStatus.BAD_REQUEST, errorMessages.INVALID_TOKEN);
      } else {
        req.user = decoded;
        next();
      }
    })
  } else {
    throw createError(httpStatus.UNAUTHORIZED, errorMessages.UNAUTHORIZED);
  }

}

const realtorProtected = (req, res, next) => {
  if (req.user.role !== config.roles.realtor) {
    throw createError(httpStatus.FORBIDDEN, errorMessages.ONLY_REALTOR);
  }
  next()
}

module.exports = {
  protected,
  realtorProtected
}

