const multer = require('multer');
const config = require('../config')

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
      cb(null, config.uploadDirectory);
  },
  filename: (req, file, cb) =>  {
      cb(null, Date.now() + '-' + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype.startsWith('image/')) {
      cb(null, true);
  } else {
      cb(new Error('Only image files are allowed'));
  }
};

const upload = multer({ storage: fileStorage, fileFilter });

module.exports = upload
