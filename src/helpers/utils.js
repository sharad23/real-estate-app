const _ = require('lodash')
const fs = require('fs').promises;
const queryString = require('querystring');
const path = require('path');
const config = require('../config');
const { Model } = require('sequelize');


const routeNameToValidationSchema = (path, method) => {
  return `${_.camelCase(path.split('/'))}${_.capitalize(method)}Schema` ;
}

const deleteUploadedFiles = async (filePaths) => {
  try {
    // Create an array of promises to delete each file
    const deletePromises = filePaths.map(filePath => fs.unlink(filePath));

    // Wait for all promises to complete
    await Promise.all(deletePromises);

    console.log('Files deleted successfully');
  } catch (err) {
    console.error('Error deleting files:', err);
  }
}

const milesToMeters = (miles) => {
  return miles * 1609.34;
} 

const kmsToMeters = (km) => {
  return km * 1000;
}

const preparePaginationPageLink = (queryParams) => {
  const pageInfoQueryParams = queryString.encode(queryParams)
  return pageInfoQueryParams
}

const preparePropertyResponse = (properties) => {
  const results = _.map(properties, (property) => {
    let propertyJSON;
    if (property instanceof Model)
      propertyJSON = property.toJSON()
    else {
      propertyJSON = property
    }
    
    if (propertyJSON.images.length > 0) {
      propertyJSON.preview_image = path.join(config.uploadDirectory, property.images[0])
    } 
    delete propertyJSON.images
    return propertyJSON 
  })

  return results
}

const prepareFiltersForSearch = (searchBody, inputs) => {
  const { geo, price_per_month } = inputs
  if (geo) {
    const unit = geo.unit || 'km'
    searchBody.query.bool.filter = {
      "geo_distance": {
        "distance": `${geo.distance}${unit}`,
        "coordinate": {
          "lat": geo.latitude,
          "lon": geo.longitude
        }
      }
    }
    searchBody.script_fields = {
      "actual_distance": {
        "script": {
          "source": "doc['coordinate'].arcDistance(params.origin_lat, params.origin_lon)", // Calculate arc distance
          "params": {
            "origin_lat": geo.latitude,   // Origin latitude
            "origin_lon": geo.longitude   // Origin longitude
          }
        }
      }
    }
  }

  if (price_per_month) {
    if (price_per_month.minimum) {
      searchBody.query.bool.must.push({"range": {"price_per_month": {"gt": price_per_month.minimum}}})
    }
    if (price_per_month.maximum) {
      searchBody.query.bool.must.push({"range": {"price_per_month": {"lt": price_per_month.maximum}}})
    }
  }

  return searchBody
}

const sleep = (ms) => {
  return new Promise(resolve => setTimeout(resolve, ms));
}

module.exports = {
  routeNameToValidationSchema,
  deleteUploadedFiles,
  milesToMeters,
  kmsToMeters,
  preparePaginationPageLink,
  preparePropertyResponse,
  prepareFiltersForSearch,
  sleep
}