module.exports = {
  NOT_FOUND: 'The requested resource was not found.',
  SERVER_ERROR: 'Internal server error occurred.',
  UNAUTHORIZED: 'Unauthorized access. Please authenticate.',
  ONLY_REALTOR: 'Route only accessible to realtor',
  ONLY_OWNER_REALTOR: 'Route only accessible to realtor owner',
  INVALID_TOKEN: 'Token is Invalid',
  DOES_NOT_EXISTS: 'Resource doesn\'t exists',
  DUPLICATE_ENTRY: 'An entry already exists with the name'
};