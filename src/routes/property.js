const express = require('express');
const { create, list, details, remove, update } = require('../lib/property');
const { legacySearch, dynamicSearch } = require('../lib/search');
const upload = require('../helpers/fileUploader');
const { realtorProtected } = require('../middlewares/protectedRoutes');

const router = express.Router();

// search routers
router.get('/legacy-search', legacySearch)
router.get('/search', dynamicSearch)

router.post('/', realtorProtected, upload.array('files', 10), create)
router.get('/', list)
router.get('/:id', details)
router.delete('/:id', realtorProtected, remove)
router.put('/:id', realtorProtected, upload.array('files', 10), update)

module.exports = router