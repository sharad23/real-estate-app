const express = require('express');
const authRoutes = require('./auth');
const propertyRoutes = require('./property')
const { protected  } = require('../middlewares/protectedRoutes')

const router = express.Router();

router.use('/auth', authRoutes);
router.use('/properties', protected, propertyRoutes);

module.exports = router;