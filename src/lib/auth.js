const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { User } = require('../models');
const config = require('../config');
const httpStatus = require('../helpers/statusCodes');
const validate = require('../validators/validate');
const createError = require('http-errors');
const { authRegisterPostSchema, authLoginPostSchema } = require('../validators/rules');

const register = async (req, res, next) => {
  try {
    validatedData = validate(authRegisterPostSchema, req.body)
    const { name, email, password, role } = validatedData
    
    // Check if user already exists
    const existingUser = await User.findOne({ where: { email } });
    if (existingUser) {
      throw createError(httpStatus.BAD_REQUEST, 'User already exists');
    }
    
    // Hash the password
    const hashedPassword = await bcrypt.hash(password, 10);

    // Create a new user
    await User.create({
      name: name.trim(),
      email,
      password: hashedPassword,
      role: config.roles[role.toLowerCase()]
    });

    res.status(httpStatus.CREATED).json({ message: 'User registered successfully' });
  } catch (error) {
    next(error)
  }
  
};

const login = async (req, res, next) => {
  try {
    validatedData = validate(authLoginPostSchema, req.body)
    const { email, password } = validatedData;

    // Check if user exists
    const user = await User.findOne({ where: { email } });
    if (!user) {
      throw createError(httpStatus.BAD_REQUEST, 'Invalid email or password');
    }

    // Verify password
    const isPasswordValid = await bcrypt.compare(password, user.password);
    if (!isPasswordValid) {
      throw createError(httpStatus.UNAUTHORIZED, 'Invalid email or password');
    }

    // Generate JWT token
    const token = jwt.sign({ id: user.id, role: user.role }, config.salt, { expiresIn: '8h' });
    res.cookie('token', token, { httpOnly: true });
    res.status(httpStatus.OK).json({message: 'ok' });
  } catch (err) {
    next(err)
  }
  
};

module.exports = { register, login };
