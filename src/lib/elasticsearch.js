const { client, indexName } = require('../elasticsearch');
const logger = require('../lib/logger')

async function createRecord(id, document) {
  try {
    const { body } = await client.index({
      index: indexName,
      id: id,
      body: document
    });
    logger.info('Record created:', body);
  } catch (error) {
    logger.error('Error creating record:', error);
  }
}

async function deleteRecord(id) {
  try {
    const { body } = await client.delete({
      index: indexName,
      id: id
    });
    logger.info('Record deleted:', body);
  } catch (error) {
    logger.error('Error deleting record:', error);
  }
}

async function updateRecord(id, updateData) {
  try {
    const { body } = await client.update({
      index: indexName,
      id: id,
      body: {
        doc: updateData
      }
    });
    logger.info('Record deleted:', body);
  } catch (error) {
    logger.error('Error deleting record:', error);
  }
}

module.exports = {
  createRecord,
  deleteRecord,
  updateRecord
}