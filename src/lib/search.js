const sequelize = require('../db');
const utils = require('../helpers/utils');
const { QueryTypes } = require('sequelize');
const validate = require('../validators/validate');
const { searchSchema } = require('../validators/rules');
const { client: elasticCLient, indexName } = require('../elasticsearch');
const _ = require('lodash');
const logger = require('./logger');

const searchViaElasticSearch = async (offset, limit, filters) => {
  try {
    const validatedData = validate(searchSchema, JSON.parse(filters || "{}"))
    const searchParams = {
      "_source": true,
      "from": offset,          
      "size": limit,
      "query": {
        "bool": {
          "must": []
        }
      },
      "track_total_hits": true
    }
    
    const searchParamsWithFilter = utils.prepareFiltersForSearch(searchParams, validatedData)
    const { body } = await elasticCLient.search({
      index: indexName,
      body: searchParamsWithFilter
    });
    const records = body.hits.hits.map((element) => {
      const result = {...element._source}
      result.uuid = element._id
      if (element.fields) {
        result.distance = element.fields.actual_distance[0]
      }
      return result
    })
  
    const results = utils.preparePropertyResponse(records)
    return { results: results, total: body.hits.total.value }
  } catch (err) {
    logger.error(err.message)
    return { results: [], total: 0}
  }
}

const searchViaMysql = async (offset, limit, filters) => {
  const validatedData = validate(searchSchema, JSON.parse(filters || "{}"))
  const { geo, price_per_month, area } = validatedData
    
  const conditions = []
  let sortField = 'id'
  const selectFields = ['id', 'name', 'uid', 'area', 'images', 'number_of_rooms', 'price_per_month', 'coordinates']
  let replacements = {}

  if (geo) {
    if (geo.longitude && geo.latitude && geo.distance) {
      const unit = geo.unit || 'km'
      let distance = utils.kmsToMeters(geo.distance)
      if (unit === 'mi') {
        distance = utils.milesToMeters(geo.distance)
      }
      conditions.push('ST_Distance_Sphere(coordinates, POINT(:long, :lat)) <= :distance')
      replacements = {...replacements, long: geo.longitude, lat: geo.latitude, distance }
      selectFields.push('ST_Distance_Sphere(coordinates, POINT(:long, :lat)) as distance')
      sortField = 'distance'
    }
  }

  if (price_per_month) {
    
    if (price_per_month.minimum) {
      conditions.push('price_per_month >= :price_per_month_min')
      replacements = {...replacements, price_per_month_min: price_per_month.minimum}
    }
    
    if (price_per_month.maximum) {
      conditions.push('price_per_month <= :price_per_month_max')
      replacements = {...replacements, price_per_month_max: price_per_month.maximum}
    }
  }

  if (area) {
    if (area.minimum) {
      conditions.push('area >= :area_min')
      replacements = {...replacements, area_min: area.minimum }
    }
    
    if (area.maximum) {
      conditions.push('area <= :area_max')
      replacements = {...replacements, area_max: area.maximum }
    }
  }
  
  if (conditions.length === 0) {
    conditions.push('1')
  } 
  
  const query = `
      SELECT  ${selectFields.join(', ')}
      FROM properties
      WHERE ${conditions.join(' AND ')}
      ORDER BY ${sortField}
      LIMIT ${limit} OFFSET ${offset}`;

  const countQuery = `
      SELECT count(id) as total
      FROM properties
      WHERE ${conditions.join(' AND ')}`
  
  
  let results = await sequelize.query(query, {
    type: QueryTypes.SELECT,
    replacements
  });
    
  results = utils.preparePropertyResponse(results)
  const totalResults = await sequelize.query(countQuery, {
    type: QueryTypes.SELECT,
    replacements
  });

  return {results, total: totalResults[0].total}
}

const legacySearch =  async (req, res, next) => {
  try {
    const page = parseInt(req.query.page) || 1;
    const limit = parseInt(req.query.pageSize) || 10;
    const offset = (page - 1) * limit;
    
    let searchResults = await searchViaMysql(offset, limit, req.query.filters)
    const hasNextPage = offset + searchResults.results.length < searchResults.total;
    const hasPrevPage = page > 1;

    return res.json({
      page: page,
      pageSize: limit,
      results: searchResults.results,
      nextPage: hasNextPage ? utils.preparePaginationPageLink({...req.query, page: page + 1}) : null,
      prevPage: hasPrevPage ? utils.preparePaginationPageLink({...req.query, page: page - 1}) : null
    });

  } catch (err) {
    next(err)
  }
}

const dynamicSearch = async (req, res, next) => {
  try {
    const page = parseInt(req.query.page) || 1;
    const limit = parseInt(req.query.pageSize) || 10;
    const offset = (page - 1) * limit;
    
    let searchResults = await searchViaElasticSearch(offset, limit, req.query.filters)
    if (searchResults.total === 0) {
      logger.info(`Searching from fallback mysql`)
      searchResults = await searchViaMysql(offset, limit, req.query.filters)
    }
    // paginated response
    const hasNextPage = offset + searchResults.results.length < searchResults.total;
    const hasPrevPage = page > 1;
    return res.json({
      page: page,
      pageSize: limit,
      results: searchResults.results,
      nextPage: hasNextPage ? utils.preparePaginationPageLink({...req.query, page: page + 1}) : null,
      prevPage: hasPrevPage ? utils.preparePaginationPageLink({...req.query, page: page - 1}) : null
    });
  
  } catch (err) {
    next(err)
  }
}

module.exports = {
  legacySearch,
  dynamicSearch
}