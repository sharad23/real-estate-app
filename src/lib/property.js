const config = require('../config')
const { v4: uuidv4 } = require('uuid');
const Property = require('../models/property');
const sanitizeHtml = require('sanitize-html');
const _ = require('lodash');
const path = require('path');
const { deleteUploadedFiles, preparePaginationPageLink, preparePropertyResponse } = require('../helpers/utils')
const httpStatus = require('../helpers/statusCodes');
const errorMessages = require('../helpers/errorMessages');
const validate = require('../validators/validate');
const { propertiesPostSchema, propertiesPutSchema } = require('../validators/rules')
const { publishToMq } = require('./mq');
const createError = require('http-errors');

const create = async (req, res, next) => {
  try {
    const validatedBody = validate(propertiesPostSchema, req.body)
    const { name, number_of_rooms, area, longitude, latitude,  description, tags, price_per_month } = validatedBody;
    const filenames = req.files.map(file => file.filename);

    // Check the property is unique
    const existingProperty = await Property.findOne({
      where: {
        name: name
      }
    })
    if (existingProperty) {
      throw createError(httpStatus.BAD_REQUEST, errorMessages.DUPLICATE_ENTRY)
    }
    
    // Create the Property record
    const property = await Property.create({
        realtor_id: req.user.id,
        name,
        uid: uuidv4(),
        area,
        number_of_rooms,
        images: filenames,
        coordinates: {
          type: 'Point',
          coordinates: [longitude, latitude]
        },
        description: description ? sanitizeHtml(description) : null,
        tags: tags ? sanitizeHtml(tags).split(',') : [],
        price_per_month
    });

    publishToMq({method: 'create', uid: property.uid, payload: {name, area, number_of_rooms, price_per_month, images: filenames, tags, coordinate: {lat:latitude, lon: longitude}} })
    res.status(httpStatus.CREATED).json({uid: property.uid});
  
  } catch (error) {
    next(error)
  }
  
}

const list = async (req, res, next) => {
  try {
    const page = parseInt(req.query.page) || 1;
    const limit = parseInt(req.query.pageSize) || 10;
    const offset = (page - 1) * limit;
    const filterCondition = { is_active: true}
    if (req.user.role === config.roles.realtor) {
      filterCondition['realtor_id'] = parseInt(req.user.id)
    }
    const properties = await Property.findAll({
      where: filterCondition,
      attributes: ['uid', 'name', 'area', 'images', 'number_of_rooms', 'price_per_month', 'coordinates'],
      limit,
      offset
    })
    
    // show only first image
    const results = preparePropertyResponse(properties)
    
    const totalCount = await Property.count({ where: filterCondition });
    const hasNextPage = offset + properties.length < totalCount;
    const hasPrevPage = page > 1;

    return res.json({
      page: page,
      pageSize: limit,
      results,
      nextPage: hasNextPage ? preparePaginationPageLink({...req.query, page: page + 1}) : null,
      prevPage: hasPrevPage ? preparePaginationPageLink({...req.query, page: page - 1}) : null
    });
  } catch(err) {
    next(err)
  }
  
}

const details = async (req, res, next) => {
  try {
    const propertyId = req.params.id
    const property = await Property.findOne({
      where: {
        uid: propertyId,
        is_active: true
      },
      attributes: { exclude: ['id'] }
    })
    
    if (!property) {
      throw createError(httpStatus.BAD_REQUEST, errorMessages.DOES_NOT_EXISTS);
    }
    property.images = _.map(property.images, (img) => path.join(config.uploadDirectory, img))
    return res.json(property)
  } catch (err) {
    next(err)
  }
  
}

const remove = async (req, res, next) => {
  try {
    const propertyId = req.params.id
    const property = await Property.findOne({where: {
      uid: propertyId
    }})
    
    if (!property) {
      throw createError(httpStatus.BAD_REQUEST, errorMessages.DOES_NOT_EXISTS);
    }
    
    if (property.realtor_id !== req.user.id) {
      throw createError(httpStatus.FORBIDDEN, errorMessages.ONLY_OWNER_REALTOR);
    }

    await Property.destroy({
      where: {
        uid: propertyId
      }
    })
    
    publishToMq({method: 'delete', uid: property.uid })

    return res.json({message: "success"})
  } catch (err) {
    next(err)
  }
    
}

const update = async (req, res, next) => {
  try {
    const validatedBody = validate(propertiesPutSchema, req.body)
    const { name, number_of_rooms, area, description, tags, price_per_month, existing_files } = validatedBody
    const property = await Property.findOne({
      where: {
        uid: req.params.id
      }
    })
    
    if (!property) {
      throw createError(httpStatus.BAD_REQUEST, errorMessages.DOES_NOT_EXISTS);
    }
    
    if (property.realtor_id !== req.user.id) {
      throw createError(httpStatus.FORBIDDEN, errorMessages.ONLY_OWNER_REALTOR);
    }
    
    // files to be deleted from disk storage
    let filesTobeDeleted = [];
    
    // files to be synced on db 
    let filesTobeSynced = property.images
    
    if (existing_files) {
      filesTobeDeleted = _.difference(property.images, existing_files.split(','))
      filesTobeSynced = _.intersection(property.images, existing_files.split(','))
    }
    
    const filesUploaded = _.map(req.files, file => file.filename);
    filesTobeSynced.push(...filesUploaded)

    // check for other fields
    const updateData = {
      name,
      number_of_rooms,
      area,
      description: description ? sanitizeHtml(description.trim()) : undefined,
      tags: tags ? sanitizeHtml(tags).split(',') : undefined,
      price_per_month,
      images: filesTobeSynced
    };
    
    Object.keys(updateData).forEach(key => updateData[key] === undefined && delete updateData[key]);
    if (validatedBody.latitude && validatedBody.longitude) {
      updateData.latitude = validatedBody.latitude
      updateData.longitude = validatedBody.longitude
    }

    await Property.update(updateData, {
      where: {
        uid: req.params.id
      }
    })

    res.json({message: "success"})
    
    // delete the files from the storage
    if (filesTobeDeleted.length > 0) {
      await deleteUploadedFiles(filesTobeDeleted.map((filePath) => path(config.uploadDirectory, filePath)))
    }
  } catch (err) {
    next(err)
  }
  
}

module.exports = {
  create,
  list,
  details,
  remove,
  update
}