const amqp = require('amqplib');
const config = require('../config');
const logger = require('../lib/logger');

const publishToMq = async (jsonData) => {
  try {
    const connection = await amqp.connect(`amqp://${config.rabbitMq.host}`);
    // Create a channel
    const channel = await connection.createChannel();
    
    const queueName = config.rabbitMq.queueName;
    await channel.assertQueue(queueName, { durable: false });

    const jsonString = JSON.stringify(jsonData);
    channel.sendToQueue(queueName, Buffer.from(jsonString));

    logger.info(`Published to queue ${jsonString}`);

    // Close the channel and connection
    await channel.close();
    await connection.close();
  } catch (err) {
    logger.error(`Error publishing in queue, ${err.message}`)
  }
}

module.exports = {
  publishToMq
}