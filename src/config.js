const roles = {
  'realtor': 'Realtor',
  "user": 'User'
}
const salt = process.env.SECRET_SALT || 'my-secret'
const uploadDirectory = process.env.UPLOAD_DIRECTORY || './uploads/'

const db = {
  host: process.env.DB_HOST_NAME || 'localhost',
  port: process.env.DB_PORT || '3306',
  username: process.env.DB_USER_NAME || 'root',
  password: process.env.DB_USER_PASSWORD || 'my-secret-pw'
}

const elasticSearch = {
  host: process.env.ES_HOST || 'localhost',
  port: process.env.ES_PORT || '9200'
}

const rabbitMq = {
  host : process.env.MQ_HOST || 'localhost',
  queueName: process.env.QUEUE_NAME || 'es-sync'
}

module.exports = {
  roles,
  salt,
  uploadDirectory,
  db,
  elasticSearch,
  rabbitMq
}