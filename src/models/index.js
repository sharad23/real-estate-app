const sequelize = require('../db');
const { checkElasticsearchHealth, createIndex } = require('../elasticsearch');
const User = require('./user');
const Property = require('./property');
const logger = require('../lib/logger');
const { sleep } = require('../helpers/utils');

(async () => {
  // connect to mysql
  let tries = 10
  while (tries > 0) {
    try {
      await sleep(5000)
      await sequelize.sync();
      logger.info('DB synchronized successfully');
      break
    } catch (error) {
      logger.error(`Error in syncronizing db ${error.message}`)
      tries -= 1
    }
  }
  if (tries === 0) {
    logger.error(`Couldnot connect to mysql`)
  }
  
  // connect to elasticsearch
  logger.info('Connecting to elasticsearch')
  let esTries = 30

  while (esTries > 0) {
    const status = await checkElasticsearchHealth();
    if (status) { 
      await createIndex()
      break
    }
    await sleep(1000)
    esTries -= 1
  }
})()

module.exports = {
  User,
  Property
}