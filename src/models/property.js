const { DataTypes } = require('sequelize');
const sequelize = require('../db');

const User = require('./user');
const Property = sequelize.define('property', {
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  uid: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  number_of_rooms: {
    type: DataTypes.INTEGER,
    allowNull: true,
  },
  area: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  coordinates: {
    type: DataTypes.GEOMETRY('POINT'),
    allowNull: false,
  },
  images: {
    type: DataTypes.JSON,
    allowNull: true
  },
  description: {
    type: DataTypes.TEXT,
    allowNull: true
  },
  is_active: {
    type: DataTypes.BOOLEAN,
    defaultValue: true
  },
  is_promoted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  tags: {
    type: DataTypes.JSON,
    allowNull: true
  },
  price_per_month: {
    type: DataTypes.INTEGER,
    allowNull: true
  } 
}, 
{
  timestamps: true,
  createdAt: 'created_at',
  updatedAt: 'updated_at',
  indexes: [
    {
      unique: false, // Example of a non-unique index
      fields: ['coordinates'],
    },
    // You can define more indexes here...
  ],
});

Property.belongsTo(User, {
  foreignKey: {
    name: 'realtor_id'
  }});

module.exports = Property;
